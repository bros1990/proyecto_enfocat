package com.enfocat.audiolibro.repositories;

import com.enfocat.audiolibro.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
